<html>
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
    <?php
      $nome = 'Mauricio';

      echo 'Hello ' . $nome . '<br />';
      echo "Hello $nome <br />";

      $num1 = 10; 
      $num2 = 30;

      echo $num1 + $num2 . ' <br />';
      echo $num1 - $num2 . ' <br />';
      echo $num1 * $num2 . ' <br />';
      echo $num1 / $num2 . ' <br />';

      $lista = [];
      $lista[] = 'João';
      $lista[] = 'Maria';
      $lista[] = 'Mohammed';
      
      $lista_invertida = array_reverse($lista);

      for($i = 0; $i < count($lista); $i++) {
        echo "O nome é: $lista[$i] <br />";
      }

      foreach($lista_invertida as $item) {
        echo "O nome é: $item <br />";
      }

      $teste = true;

      /*  == - igual
          != - diferente
          || - ou
          && - e
          or - ou com baixa precedência
          and - e com baixa precedência
          === - igual (verificando valor e tipagem)

      */
      if ($teste === 1) {
        echo 'É verdadeiro <br />';
      } else {
        echo 'É falso <br />';        
      }

      echo 'A data e hora atual é: ' . date("d/m/Y H:i:s");

    ?>
  </body>
</html>
