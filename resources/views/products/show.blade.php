@extends('welcome')

@section('content')
  <h1>Detalhes do Produto</h1>

  <dl class="dl-horizontal">
    <dt>Nome</dt>
    <dd>{{ $produto->nome }}</dd>
    <dt>Valor</dt>
    <dd>R$ {{ $produto->valor }}</dd>
    <dt>Criado em</dt>
    <dd>{{ $produto->created_at->format('d/m/Y h:i') }}</dd>
    <dt>Atualizdo em</dt>
    <dd>{{ $produto->updated_at->format('d/m/Y h:i') }}</dd>
  </dl>
@endsection