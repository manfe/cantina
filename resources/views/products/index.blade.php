@extends('welcome')

@section('content')
  <div class="row">
    <h1>Produtos</h1>
    <hr />

    <a href="/produtos/create" class="btn btn-success pull-right">
      Novo Produto
    </a>
    <br />
    <br />
    <br />
  </div>
  <div class="row">

    @if(Session::has('message'))
      <div class="alert alert-success">
        <em> {!! session('message') !!}</em>
      </div>
    @endif

    <table class="table table-bordered">
      <tr>
        <th>ID</th>
        <th>Nome</th>
        <th>Valor</th>
        <th>Ações</th>
      </tr>
      @foreach($produtos as $produto)
        <tr>
          <td>{{ $produto->id }}</td>
          <td>{{ $produto->nome }}</td>
          <td>{{ $produto->valor }}</td>
          <td>
            <a href="/produtos/{{ $produto->id }}" class="btn btn-default" aria-label="Mostrar Produto">
              <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
            </a>
          </td>
        </tr>
      @endforeach
    </table>
  </div>
@endsection