# Projeto Cantina

Esse projeto tem por objetivo avaliar o processo de desenvolvimento de um software na disciplina de Engenharia de Software do curso de Ciência da Computação do Instituto Federal Catarinense - Campus Videira.

Para executar esse projeto execute:

```php -S localhost:8000```

## Dependências

* PHP >= 5.5
* Mysql Server >= 5.5.54


